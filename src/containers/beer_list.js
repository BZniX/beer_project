import React, {Component} from 'react';
import {getBeers} from "../actions/index";
import {getBeersDetails, emptyStock} from "../actions/index";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import BeerListItem from "../components/beer-list-item";

class BeerList extends Component {

    componentWillMount() {
        this.props.getBeers();
    }
    
    getinfos(name, stock) {
	    this.props.emptyStock(name);
	    if (stock > 0) {
		    this.props.getBeersDetails(name);
	    }
    }
    
    renderBeers() {
        const {beers} = this.props;
        if(beers) {
            return (
                <div>
                    {
                        beers.map((data) => {
                            return <BeerListItem key={data.beerId} beers={data} onClick={() => this.getinfos(data.beerName, data.quantities)}/> //onClick : onChange seulement sur les éléments du DOM
                        })
                    }
                </div>
            )
        }
    }
    
    render() {
        return(
            <div>
                {this.renderBeers()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        beers: state.beers
    }
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getBeers, getBeersDetails, emptyStock}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(BeerList);