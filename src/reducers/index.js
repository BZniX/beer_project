import { combineReducers } from 'redux';
import reducerBeers from './reducer-beers';
import reducerSelectedBeers from './reducer-selected-beers';

const rootReducer = combineReducers({
    beers:reducerBeers,
    selectedBeers:reducerSelectedBeers
});

export default rootReducer;
