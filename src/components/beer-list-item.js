import React from 'react';

const BeerListItem = (props) => {

    return (
    	<div {...props}>
		    {
		    	(props.beers.quantities < 1)
		    		? <button style={{fontSize:'1.2em', color:'white', backgroundColor:'grey', width:'200px', textAlign:'center', borderRadius:'7px',display:'block', margin:'auto'}} >
						    <p>{props.beers.beerName}</p>
						    <p>{props.beers.quantities}</p>
					    </button>
				    : <button style={{fontSize:'1.2em', color:'white', backgroundColor:'blue', width:'200px', textAlign:'center', borderRadius:'7px',display:'block', margin:'auto'}} >
						    <p>{props.beers.beerName}</p>
						    <p>{props.beers.quantities}</p>
					    </button>
		    }
	    </div>
    );
};

export default BeerListItem;
