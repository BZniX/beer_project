import React, { Component } from 'react';
import BeerList from '../containers/beer_list';
import BeerDetails from '../containers/beer_details';

class App extends Component {
    
    render() {
        return (
            <div className="App">
                <BeerList/>
	            <BeerDetails/>
            </div>
            
        );
    }
}

export default App;
