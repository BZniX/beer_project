import { GET_BEERS } from '../actions/index.js'
import { EMPTY_STOCK_BEERS } from "../actions";
import { FILL_STOCK_BEERS } from "../actions";
import { RESET_BEERS } from "../actions";

function emptyStockChange(state, action) {
	let newList = state
	for (let i = 0; i < newList.length; i++) {
		if (newList[i].beerName === action.payload) {
			if(newList[i].quantities > 1) {
				newList[i].quantities--;
			} else {
				newList[i].quantities = 0;
			}
		}
	}
	return newList
}

function fillStockchange(state, action) {
	let newList = state
	for (let i = 0; i < newList.length; i++) {
		if (newList[i].beerName === action.payload) {
			newList[i].quantities++;
		}
	}
	return newList
}

export default function(state = null, action) {
    switch(action.type) {
        case GET_BEERS: return action.payload;
	    case RESET_BEERS: return action.payload;
	    case EMPTY_STOCK_BEERS:
		    emptyStockChange(state, action);
		    console.log('stock : ', state);
	    	return [...state];
	    case FILL_STOCK_BEERS:
		    fillStockchange(state, action);
		    return [...state]
	    default : return state
    }
}