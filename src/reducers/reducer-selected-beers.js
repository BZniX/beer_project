import { SELECT_BEERS } from '../actions/index.js'
import { DELETE_BEERS } from '../actions/index.js'
import { RESET_BEERS } from '../actions/index.js'

function makeTab(state, action) {
	let newItem = true;
	let newList = state;
	for (let i = 0; i < newList.length; i++) {
		if (state[i].name === action.payload) {
			newList[i].quantity++;
			newItem = false;
		}
	}
	if (newItem) newList.push({name: action.payload, quantity: 1});
	return newList
}

function deleteBeer(state, action) {
	let newList = state;
	for (let i = 0; i < newList.length; i++) {
		if (state[i].name === action.payload) {
			newList[i].quantity--;
			if (newList[i].quantity < 1 ) newList.splice(i, 1);
		}
	}
	return newList
}

export default function(state = [], action) {
	switch(action.type) {
		case SELECT_BEERS :
			makeTab(state, action);
			return [...state]
		case DELETE_BEERS :
			deleteBeer(state, action);
			return [...state]
		case RESET_BEERS :
			return []
		default :
			return state
	}
}
