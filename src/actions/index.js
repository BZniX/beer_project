import axios from 'axios';

export const GET_BEERS = "GET_BEERS";
export const SELECT_BEERS = "SELECT_BEERS";
export const DELETE_BEERS = "DELETE_BEERS";
export const RESET_BEERS = "RESET_BEERS";
export const EMPTY_STOCK_BEERS = "EMPTY_STOCK_BEERS";
export const FILL_STOCK_BEERS = "FILL_STOCK_BEERS";
export const RESET_LIST = "RESET_LIST";

const API_END_POINT = "https://coucouval.herokuapp.com";

export function getBeers() {
    return function(dispatch) {
        axios.get(`${API_END_POINT}/beersStore`)
            .then(function(response) {
                dispatch({type: GET_BEERS, payload:response.data.beersStore});
            })
    }
}

export function getBeersDetails(name) {
    return {
    	type : SELECT_BEERS,
	    payload : name
    }
}

export function getDeletedBeers(name) {
	return {
		type : DELETE_BEERS,
		payload : name
	}
}

export function resetBeers() {
	return function(dispatch) {
		axios.get(`${API_END_POINT}/beersStore`)
			.then(function(response) {
				dispatch({type: RESET_BEERS, payload:response.data.beersStore});
			})
	}
}

export function emptyStock(name) {
	return {
		type : EMPTY_STOCK_BEERS,
		payload : name
	}
}

export function fillStock(name) {
	return {
		type : FILL_STOCK_BEERS,
		payload : name
	}
}