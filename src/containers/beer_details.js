import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/beerList.css';
import {getDeletedBeers, resetBeers, fillStock} from "../actions";
import {bindActionCreators} from "redux";

class BeerDetails extends Component {
	
	deleteB(name, stock) {
		this.props.fillStock(name)
		if (stock > 0) {
			this.props.getDeletedBeers(name);
		}
	}
	
	resetB() {
		this.props.resetBeers();
	}
	
	render() {
		if (this.props.SELECTED.length !== 0) {
			return(
				<div>
					<button className="beerList" onClick={() => this.resetB()}>RESET</button>
					{
						this.props.SELECTED.map((data, i) => {
							if(typeof data === "object") {
							return (
								<div key={i} className="beerList">
									<div>
										<h3>{data.name} - {data.quantity}</h3>
									</div>
									<button onClick={() => this.deleteB(data.name, data.quantity)}>Delete</button>
								</div>
							);
							}
						})
					}
				</div>
			)
		} else {
			return(
				<div>
					<p  className="select">Select one</p>
				</div>
			)
		}
    }
}

function mapStateToProps(state) {
    return {
	    SELECTED: state.selectedBeers
    }
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({getDeletedBeers, resetBeers, fillStock}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BeerDetails);
